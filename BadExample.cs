using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class BadExample : MonoBehaviour
{
    [SerializeField] private Text _timerText;

    private float _timerValue;

    private void Update()
    {
        _timerValue += Time.deltaTime;
        _timerText.text = "Time:" + _timerValue.ToString();
    }
}
