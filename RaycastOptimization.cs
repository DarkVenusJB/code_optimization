using UnityEngine;

public class RaycastOptimization : MonoBehaviour
{
    [SerializeField] private float _speed = 5.0f;
    [SerializeField]private float _interval = 1f; 

    private bool _hasHit = false;
    private float _timer;
    private float _timeTillImpact;
    private Vector3 _begin;
    private RaycastHit _hit;
     
    private void Start()
    {
        _begin = transform.position;
        _timer = _interval + 1;
    }

    private void Update()
    {       
        float usedInterval = _interval;

        if (Time.deltaTime > usedInterval)
        {
            usedInterval = Time.deltaTime;
        } 
              
        if (!_hasHit && _timer >= usedInterval)
        {
            _timer = 0;
            int distanceThisInterval = (int)(_speed * usedInterval);
            Debug.Log("TryHit");

            if (Physics.Raycast(_begin, transform.forward, distanceThisInterval))
            {
                _hasHit = true;
                if (_speed != 0)
                {
                    _timeTillImpact = _hit.distance / _speed;
                }               
            }
            _begin += transform.forward * distanceThisInterval;
        }

        _timer += Time.deltaTime;
       
        if (_hasHit && _timer > _timeTillImpact)
        {
            // Do hit
        }
        else
        {
            transform.position += _speed * Time.deltaTime * transform.forward;
        }
    }
}
