using UnityEngine;
using UnityEngine.UI;

public class GoodExample : MonoBehaviour
{
    [SerializeField] private Text _timerHeaderText;
    [SerializeField] private Text _timerValueText;

    private float _timerValue;

    private void Start()
    {
        _timerHeaderText.text = "Time:";
    }

    private void Update()
    {
        _timerValue += Time.deltaTime;
        _timerValueText.text = _timerValue.ToString();
    }
}
